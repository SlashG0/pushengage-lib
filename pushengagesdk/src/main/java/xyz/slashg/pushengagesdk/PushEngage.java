package xyz.slashg.pushengagesdk;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

/**
 * @author SlashG
 * @created 21/03/20
 * @since <nextVersion/>
 */
public class PushEngage {

	private static boolean isInit = false;
	@Nullable
	private static String fcmToken;
	@Nullable
	private static FCMTokenListener fcmTokenListener;

	public static void setFcmTokenListener(FCMTokenListener fcmTokenListener) {
		PushEngage.fcmTokenListener = fcmTokenListener;
	}

	private static void safelyTriggerListener(String token) {
		Log.d(TAG, "safelyTriggerListener: token received : " + token);
		if (fcmTokenListener != null) { fcmTokenListener.onFCMTokenReady(token); }
		else {
			Log.e(TAG, "safelyTriggerListener: Listener not ready, ignoring");
		}
	}

	private static final String TAG = PushEngage.class.getSimpleName();

	private static void setFcmToken(String fcmToken) {
		PushEngage.fcmToken = fcmToken;
		safelyTriggerListener(fcmToken);
	}

	protected static void deInit() {
		isInit = false;
	}

	public interface FCMTokenListener {
		void onFCMTokenReady(String token);
	}

	public static void init() {

		// more methods may be added here
		isInit = true;
	}

	/**
	 * Method that checks {@link PushEngage#isInit}
	 * if it is false, throws {@link NotInitException}
	 *
	 * @added 21/03/20
	 * @author kreatryx
	 * @since <nextVersion/>
	 */
	private static void checkInit() throws NotInitException {
		if (!isInit) { throw new NotInitException(); }
	}

	/**
	 * @return true if the device has permission to display push notification else return false.
	 * @added 21/03/20
	 * @author kreatryx
	 * @since <nextVersion/>
	 */
	public static boolean getPermissionStatus(@NonNull Context context) throws NotInitException {
		checkInit();

		//todo add more permissions as && operations
		return (ContextCompat.checkSelfPermission(context, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED);
	}


	/**
	 * Registers the device for push notification with FCM.
	 *
	 * @added 21/03/20
	 * @author kreatryx
	 * @since <nextVersion/>
	 */
	public static void subscribe() throws NotInitException {
		checkInit();
		FirebaseInstanceId.getInstance().getInstanceId()
				.addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
					@Override
					public void onComplete(@NonNull Task<InstanceIdResult> task) {
						if (task.isSuccessful() && task.getResult() != null) {
							setFcmToken(task.getResult().getToken());
						}
					}
				});
	}

	/**
	 * @return true if the device has been registered for push notification with FCM else false
	 * @added 21/03/20
	 * @author kreatryx
	 * @since <nextVersion/>
	 */
	public static boolean getSubscriptionStatus() throws NotInitException {
		checkInit();
		return fcmToken != null;
	}

	/**
	 * @return FCM push token for the device if it has not been registered for push notification else null.
	 * @added 21/03/20
	 * @author kreatryx
	 * @since <nextVersion/>
	 */
	@Nullable
	public static String getSubscriptionToken() throws NotInitException {
		checkInit();
		return fcmToken;
	}

	public static class NotInitException extends RuntimeException {
		public NotInitException() {
			super("Must call init() first");
		}
	}
}
