package xyz.slashg.pushengage;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import xyz.slashg.pushengagesdk.PushEngage;
//import PushE

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	protected void onTokenReceived(String token) {
		showToast("Token received:\n" + token);
	}

	public void init(View view) {
		initFcmSdk();
		showToast("Inited");
	}

	public void subscribe(View view) {
		try {
			PushEngage.subscribe();
			showToast("Subscribing");
		} catch (PushEngage.NotInitException e) {
			showToast("Exception : init must be called");
		}
	}

	public void getPermissionStatus(View view) {
		try {
			showToast("Permission available : " + PushEngage.getPermissionStatus(this));
		} catch (PushEngage.NotInitException e) {
			showToast("Exception : init must be called");
		}
	}

	public void getSubscriptionStatus(View view) {
		try {
			showToast("Subscription done : " + PushEngage.getSubscriptionStatus());
		} catch (PushEngage.NotInitException e) {
			showToast("Exception : init must be called");
		}
	}

	public void getSubscriptionToken(View view) {

		try {
			showToast("Token : " + PushEngage.getSubscriptionToken());
		} catch (PushEngage.NotInitException e) {
			showToast("Exception : init must be called");
		}

	}


	private void showToast(String message) {
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	protected void initFcmSdk() {
		PushEngage.init();
		PushEngage.setFcmTokenListener(this::onTokenReceived);
	}
}
